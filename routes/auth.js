const express = require('express')

const { body } = require('express-validator')

// const isAuth = require('../middleware/is-auth')
const authController = require('../controllers/auth')

const router = express.Router()

router.put(
  '/signup',
  [
    body('password')
      .trim()
      .not()
      .isEmpty()
      .isLength({ min: 5 })
      .withMessage('Password Must Be At Least 5 Characters.'),
    body('first_name')
      .trim()
      .not()
      .isEmpty(),
    body('last_name')
      .trim()
      .not()
      .isEmpty(),
    body('username')
      .trim()
      .not()
      .isEmpty()
      .withMessage('UserName must not be empty')
  ],
  authController.signup
)

router.post(
  '/login',
  [
    body('email')
      .isEmail()
      .withMessage('Please Enter A Valid Email Address.')
  ],
  authController.login
)

module.exports = router
