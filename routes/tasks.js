const express = require('express')
const taskController = require('../controllers/tasks')
const router = express.Router()
const isAuth = require('../middlewares/is-auth')
const { body } = require('express-validator')

router.put(
  '/create',
  [
    body('name')
      .trim()
      .not()
      .isEmpty()

      .withMessage('Task Name Is Required Field .'),
    body('description')
      .trim()
      .not()
      .isEmpty()

      .withMessage('Description Name Is Required Field .')
  ],
  isAuth,
  taskController.putCreateTask
)

router.patch(
  '/edit',
  [
    body('task_id')
      .trim()
      .not()
      .isEmpty()
      .withMessage('task_id is required field')
  ],
  isAuth,
  taskController.patchEditTask
)

router.delete(
  '/delete',
  [
    body('task_id')
      .trim()
      .not()
      .isEmpty()
      .withMessage('task_id is required field')
  ],
  isAuth,
  taskController.deleteDeleteTask
)

module.exports = router
