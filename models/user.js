const Sequelize = require('sequelize')

const sequelize = require('../util/database')

const User = sequelize.define('users', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },

  first_name: {
    type: Sequelize.STRING,
    allowNull: false
  },

  last_name: {
    type: Sequelize.STRING,
    allowNull: false
  },

  username: {
    type: Sequelize.STRING,
    allowNull: true
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false
  },

  password: {
    type: Sequelize.STRING,
    allowNull: false
  },

  jwt: {
    type: Sequelize.STRING,
    allowNull: true
  }
})
module.exports = User
