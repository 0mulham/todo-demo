const Sequelize = require('sequelize')

const sequelize = require('../util/database')
const User = require('./user')
const Task = sequelize.define('tasks', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },

  name: {
    type: Sequelize.STRING,
    allowNull: false
  },

  description: {
    type: Sequelize.STRING,
    allowNull: false
  },

  is_completed: {
    type: Sequelize.TINYINT,
    allowNull: false,
    defaultValue: 0
  }
})

Task.belongsTo(User, { as: 'TaskUser', foreignKey: 'user_id' })
User.hasMany(Task, { as: 'UserTasks', foreignKey: 'user_id' })

module.exports = Task
