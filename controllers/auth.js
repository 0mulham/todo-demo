const User = require('../models/user')
const bcrypt = require('bcryptjs')

const jwt = require('jsonwebtoken')

const { validationResult } = require('express-validator/check')

exports.signup = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const error = new Error(errors.array()[0].msg)
    error.statusCode = 442
    error.data = errors.array()
    throw error
  }
  // sign up logic

  const first_name = req.body.first_name
  const last_name = req.body.last_name
  const username = req.body.username
  const email = req.body.email
  const password = req.body.password
  bcrypt
    .hash(password, 12)
    .then(hashedPassword => {
      return User.build({
        first_name: first_name,
        last_name: last_name,
        password: hashedPassword,
        email: email,
        username: username
      })
        .save()
        .then(user => {
          return user
        })
    })
    .then(user => {
      res
        .status(201)
        .json({ message: 'User Created Successfully.', user_id: user.id })
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}

exports.login = (req, res, next) => {
  const email = req.body.email
  const password = req.body.password

  let loadedUser
  User.findOne({ where: { email: email } })
    .then(user => {
      if (!user) {
        const error = new Error('A user with this email could not be found.')
        error.statusCode = 401
        throw error
      }

      loadedUser = user
      return bcrypt.compare(password, user.password)
    })
    .then(isEqual => {
      if (!isEqual) {
        const error = new Error('Wrong password!')
        error.statusCode = 401
        throw error
      }
      const token = jwt.sign(
        {
          email: loadedUser.email,
          userId: loadedUser.id.toString()
        },
        'todoappsecret',
        { expiresIn: '1d' }
      )
      res.status(200).json({ token: token, userId: loadedUser.id.toString() })
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}
