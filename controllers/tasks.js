const User = require('../models/user')
const Task = require('../models/task')

const { validationResult } = require('express-validator/check')

exports.putCreateTask = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const error = new Error(errors.array()[0].msg)
    error.statusCode = 442
    error.data = errors.array()
    throw error
  }
  // create task logic
  //get request body params
  const name = req.body.name
  const description = req.body.description
  //get user id from auth middleware
  const userId = req.userId
  Task.build({
    name: name,
    description: description,
    user_id: userId
  })
    .save()
    .then(task => {
      return task
    })
    .then(task => {
      res
        .status(201)
        .json({ message: 'Task Created Successfully.', task_id: task.id })
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}

exports.patchEditTask = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const error = new Error(errors.array()[0].msg)
    error.statusCode = 442
    error.data = errors.array()
    throw error
  }
  var userId = req.userId
  var taskId = req.body.task_id
  var name = req.body.name
  var description = req.body.description
  var isCompleted = req.body.is_completed

  let loadedTask
  Task.findOne({ where: { id: taskId } })
    .then(task => {
      if (!task) {
        const error = new Error('Task could not be found.')
        error.statusCode = 404
        throw error
      }
      loadedTask = task
      if (name === undefined) {
        name = loadedTask.name
      } else {
        // check if there is already a task with the name provided. Supposing task name must be unique
        if (name == loadedTask.name) {
          const error = new Error('You already have a task with the same name.')
          error.statusCode = 409
          throw error
        }
      }
      if (description === undefined) {
        description = loadedTask.description
      }
      if (isCompleted === undefined) {
        isCompleted = loadedTask.is_completed
      }

      loadedTask.name = name
      loadedTask.description = description
      loadedTask.is_completed = isCompleted

      loadedTask.save().then(result => {
        if (!result) {
          const error = new Error('Something went wrong!')
          error.statusCode = 500
          throw error
        }
        res
          .status(201)
          .json({ message: 'Your task has been edited successfully.' })
      })
    })

    .catch(err => {
      console.log(err)
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}

exports.deleteDeleteTask = (req, res, next) => {
  const taskId = req.body.task_id
  const userId = req.userId
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const error = new Error(errors.array()[0].msg)
    error.statusCode = 442
    error.data = errors.array()
    throw error
  }

  Task.findOne({ where: { id: taskId } })
    .then(task => {
      if (!task) {
        // throw task not found
        const error = new Error('Task could not be found.')
        error.statusCode = 404
        throw error
      }

      if (task.user_id != userId) {
        // permission denied
        const error = new Error(
          'Cannot delete task that does not belong to you.'
        )
        error.statusCode = 403
        throw error
      }
      return Task.destroy({
        where: {
          id: taskId
        }
      })
    })
    .then(result => {
      if (!result) {
        const error = new Error('Something went wrong!')
        error.statusCode = 500
        throw error
      }

      res
        .status(200)
        .json({ message: 'Your task has been deleted successfully.' })
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500
      }
      next(err)
    })
}
