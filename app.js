const path = require('path')
const express = require('express')

const sequelize = require('./util/database')

const User = require('./models/user')
const Task = require('./models/task')
const bodyParser = require('body-parser')
const authRoutes = require('./routes/auth')
const tasksRoutes = require('./routes/tasks')

const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ limit: '50mb' }))
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  )
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
  next()
})

app.use('/tasks', tasksRoutes)
app.use('/auth', authRoutes)

app.use((error, req, res, next) => {
  console.log(error)
  const status = error.statusCode || 500
  const message = error.message
  const data = error.data
  res.status(status).json({ message: message, data: data })
})

app.use((req, res, next) => {
  res.status(404).send('<h1>Page Not Found !</h1>')
})

// app.listen(3000)

sequelize
  .sync({ alter: true })
  .then(result => {
    console.log(result)
    app.listen(3000)
  })
  .catch(err => {
    console.log(err)
  })
