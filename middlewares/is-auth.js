const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  const authHeader = req.get('Authorization')
  if (!authHeader) {
    const error = new Error('Not authenticated.')
    error.statusCode = 401
    throw error
  }
  const token = authHeader.split(' ')[0]
  let decodedToken
  try {
    decodedToken = jwt.verify(token, 'todoappsecret')
  } catch (err) {
    err.statusCode = 500
    if (err.message == 'jwt expired') {
      err.statusCode = 401
    }
    throw err
  }
  if (!decodedToken) {
    const error = new Error('Not authenticated.')
    error.statusCode = 401
    throw error
  }
  req.userId = decodedToken.userId
  next()
}

/// add in front end :  authorization 'Bearer ' + token
